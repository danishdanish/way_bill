<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('public/theme/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('public/theme/fonts/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('public/theme/css/ionicons.min.css')}}">

   <link rel="stylesheet" href="{{asset('public/theme/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/theme/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('public/theme/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" type="text/css" href="{{asset('public/theme/css/bootstrap-msg.css')}}" />

  <link href="{{asset('public/theme/css/confirmDialog.css')}}" rel="stylesheet">
  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">