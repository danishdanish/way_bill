<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('public/theme/js/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('public/theme/js/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.7 -->
<script src="{{asset('public/theme/js/bootstrap.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('public/theme/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/theme/js/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('public/theme/js/bootstrap-msg.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('public/theme/js/adminlte.min.js')}}"></script>

<script src="{{asset('public/theme/js/confirmDialog.jquery.min.js')}}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
