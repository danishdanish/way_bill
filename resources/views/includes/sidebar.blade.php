  <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('/')}}/public/theme/image/profile_photo/{{Auth::user()->profile_photo}}" class="img-circle" alt="User Image" style="width: 35px;height: 35px;">
        </div>
        <div class="pull-left info">
          <p style="margin-top: 3px;">{{Auth::user()->name}}</p>
          
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

      <li>
          <a href="{{URL::to('/')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>  
        </li> 


        <li class=" treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>User settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="{{URL::to('/user')}}"><i class="fa fa-circle-o"></i> List</a></li>
            <li><a href="{{URL::to('/user_add_page')}}"><i class="fa fa-circle-o"></i> Add</a></li>
          </ul>
        </li> 
        
      </ul>
    </section>