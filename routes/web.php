<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware'=>'authentication'],function(){
Route::get('/', function () {
    return view('pages/dashboard');
});
Route::get('/user','UserController@user_list');
Route::get('/user_add_page','UserController@user_add');
Route::post('/user/add_user','UserController@insert_user_data');
Route::get('/user/ajax_data','UserController@ajax_data');
Route::get('/user_edit/{id}','UserController@user_edit');
Route::get('/user_delete/{id}','UserController@user_delete');
Route::get('/logout','UserController@logout');
Route::get('/change_password','UserController@change_password_page');
//Route::post('/change_password/check_old_password','UserController@check_old_password');
});

Route::get('/login','LoginController@login_page');
Route::post('/check_login','LoginController@check_login');
