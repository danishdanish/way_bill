<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User ;

use Session ;

use Auth;

use Hash ;

class UserController extends Controller
{
    public function user_list()
    {
    	
    	return view('pages/user/list');
    }

    public function ajax_data()
    {
      
        $user = user::get();
        
        $i=1;

        foreach ($user as $u) {
         
        $image =  '<img width="80px" height="80px" src="'.url('public/theme/image/profile_photo/'.$u->profile_photo).'"/>';
            $row[] = array(
                           'si_no' =>$i++,
                           'name' =>$u->name,
                           'email' =>$u->email,
                           'phone' =>$u->phone_no,
                           'profile_photo' =>$image,
                           'actions' =>'<a href="user_edit/'.$u->id.'"><button class="btn btn-info" title="edit"><span class="glyphicon glyphicon-edit"></span></button></a> <button '.(count($user)<2?"disabled='disabled'":"").' title="delete" class="delete btn btn-danger"><span class="glyphicon glyphicon-remove"></span><input type="hidden" id="delete_id" value="'.$u->id.'"/></button>'
                           );

        }

         $response = array(
                       "draw" => 0,
                       "recordsTotal" => count($row),
                       "recordsFiltered" => count($row),
                       "data" => $row
    );
    echo json_encode($response);

    }

    public function user_add()
    {
    	return view('pages/user/add');
    }

    public function insert_user_data(Request $request)
    {
        if($request->edit_id>0)
        {
          $data = array(
                        'name' =>$request->name,
                        'email'=>$request->email,
                        'phone_no' =>$request->phone,
                        'username' =>$request->username,
                        'password' =>Hash::make($request->password),
                       );
           if($request->profile_photo!='')
           {

              $file = $request->profile_photo;

              $destinationPath = public_path(). '/theme/image/profile_photo';

              $filename = $file->getClientOriginalName();

              $file->move($destinationPath, $filename);

              $data['profile_photo'] = $filename;
           }

           user::where('id',$request->edit_id)->update($data);
           
           Session::flash('edit', 1);

        }
        else
        {
    	$file = $request->profile_photo;

    	 $destinationPath = public_path(). '/theme/image/profile_photo';

         $filename = $file->getClientOriginalName();

         $file->move($destinationPath, $filename);

    	$user = new user;

    	$user->name = $request->name;

    	$user->email = $request->email;

    	$user->phone_no = $request->phone;

    	$user->username = $request->username;

    	$user->password = Hash::make($request->password);

    	$user->profile_photo = $filename ;

    	$user->save();

    	Session::flash('add', 1);
       } 

    	return redirect('/user');



    }

   public function user_edit($id)
   {

      $data['data'] = user::where('id',$id)->first();
      
      return view('pages/user/edit',$data);
   } 

   public function user_delete($id)
   {

      $result = user::where('id',$id)->delete();

      return $result ;
   }

   public function logout()
   {
     
       Auth::logout();

       return redirect('/login');
   }

   public function change_password_page()
   {

      return view('pages/change_password');
   }
}
